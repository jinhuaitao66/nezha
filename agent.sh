#!/bin/bash

# 检查系统架构
ARCH=$(uname -m)
if [ "$ARCH" == "x86_64" ]; then
    DOWNLOAD_URL="https://jht126.eu.org/https://github.com/nezhahq/agent/releases/download/v1.7.3/nezha-agent_linux_amd64.zip"
elif [[ "$ARCH" == arm* || "$ARCH" == aarch64 ]]; then
    DOWNLOAD_URL="https://jht126.eu.org/https://github.com/nezhahq/agent/releases/download/v1.7.3/nezha-agent_linux_arm.zip"
else
    echo "不支持的系统架构: $ARCH"
    exit 1
fi

# 暂停服务
if systemctl is-active --quiet nezha-agent.service; then
    echo "正在停止 nezha-agent.service 服务..."
    systemctl stop nezha-agent.service
    echo "nezha-agent.service 服务已停止。"
else
    echo "nezha-agent.service 服务未运行。"
fi

# 检查文件夹是否存在
if [ ! -d "/opt/nezha/agent" ]; then
    echo "文件夹 /opt/nezha/agent 不存在，正在创建..."
    mkdir -p /opt/nezha/agent
else
    echo "文件夹 /opt/nezha/agent 已存在。"
fi

# 切换到目标目录
cd /opt/nezha/agent || exit

# 删除旧的 agent 文件
rm -f nezha-agent app.zip

# 下载并重命名为 app.zip
echo "正在下载 Nezha Agent..."
wget -O app.zip "$DOWNLOAD_URL"

# 解压 zip 文件中的内容到当前目录，并重命名为 nezha-agent
echo "正在解压文件..."
unzip -p app.zip > nezha-agent

# 给解压后的文件赋予执行权限
chmod 755 nezha-agent

# 重启服务
echo "正在重启 nezha-agent.service 服务..."
systemctl start nezha-agent.service

if systemctl is-active --quiet nezha-agent.service; then
    echo "nezha-agent.service 服务已成功重启。"
else
    echo "nezha-agent.service 服务启动失败，请检查日志。"
fi
