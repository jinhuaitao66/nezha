# 暂停 nezha-dashboard 服务
if systemctl is-active --quiet nezha-dashboard.service; then
    echo "正在停止 nezha-dashboard 服务..."
    systemctl stop nezha-dashboard.service
    echo "nezha-dashboard 服务已停止。"
else
    echo "nezha-dashboard 服务未运行。"
fi

# 检查文件夹是否存在
if [ ! -d "/opt/nezha/dashboard" ]; then
    echo "文件夹 /opt/nezha/dashboard 不存在，正在创建..."
    mkdir -p /opt/nezha/dashboard
else
    echo "文件夹 /opt/nezha/dashboard 已存在。"
fi

# 切换到目标目录
cd /opt/nezha/dashboard

# 删除旧的 app 文件
rm -f app

# 下载并重命名为 app.zip
wget -O app.zip https://jht126.eu.org/https://github.com/nezhahq/nezha/releases/download/v1.4.4/dashboard-linux-amd64.zip

# 解压 zip 文件中的内容到当前目录，并重命名为 app
unzip -p app.zip > app

# 给解压后的文件赋予执行权限
chmod 755 /opt/nezha/dashboard/app

# 重启 nezha-dashboard 服务
echo "正在重启 nezha-dashboard 服务..."
systemctl start nezha-dashboard.service
echo "nezha-dashboard 服务已重启。"
